### What is this repository for? ###

* This repository is for the barcode label maker written in Python
* Version: 0.1

### How do I get set up? ###

* pip3 install -r requirements.txt
* Dependencies:
- openpyxl - XLS reader
- reportlab - PDF generator

### How do I run the application? ###

* Put the source XLS file to the root directory of the project
* Change FILENAME constant in the beginning of the file make.py
* python3 make.py
* See the results in the tags directory that are also located in the root directory

### Who do I talk to? ###

* E-mail: ivan@menshenin.hm
* Telegram: @menshenin
