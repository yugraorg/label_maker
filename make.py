# This program creates labels with an SVG barcode and other
# necessary information as PDF files adapted for scaling and printing

import os, sys
import shutil
from io import BytesIO

from openpyxl import load_workbook

from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from reportlab.pdfbase import ttfonts
from reportlab.pdfbase.pdfmetrics import registerFont, stringWidth
from reportlab.platypus.tables import Table, TableStyle
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.barcode.eanbc import Ean13BarcodeWidget
from reportlab.graphics import renderPDF


FILENAME = 'example.xlsx'

current_dir = os.path.dirname(os.path.realpath(__file__))

def clear_tags():
    """
    Recreates tags directory in order to delete old files and directories
    """
    
    tags_dir = os.path.join(current_dir, 'tags')
    try:
        shutil.rmtree(tags_dir)
    except FileNotFoundError:
        pass
    
    os.mkdir(tags_dir)
    return None
    

def get_file():
    """
    Searches for source file and returns its path if it exists
    """
    file_path = os.path.join(current_dir, FILENAME)
    
    try:
        f = open(file_path, 'rb')
    except:
        print(f'Can\'t find a file {FILENAME}')
        sys.exit()
    
    return file_path


def get_data(file_path):
    """
    Opens the given source file and returns a data structure to work
    """
    data = {'titles': [], 'values': []}
    
    # load Excel file
    try:
        wb = load_workbook(file_path)
    except:
        print('Cannot read a workbook, something is wrong with the file.')
        sys.exit()
    
    ws = wb.active
       
    # create titles
    col = 1
    cell = ws.cell(row=1, column=1)
    
    while cell.value:
        data['titles'].append(cell.value)
        col += 1
        cell = ws.cell(row=1, column=col)
    
    # create values
    max_cols = len(data['titles'])
    rows = tuple(ws.rows)[1:]
    
    for r in rows:
        values = []
        for c in range(max_cols):
            values.append(r[c].value)
        data['values'].append(values)    
    
    return data


def make_pdf(titles, values):
    """
    Creates a PDF file based on titles and values
    """
    
    pdf_file = BytesIO()
    width, height = A4
    space = round(width*0.1)
    cnvs = canvas.Canvas(pdf_file, pagesize=A4)
    
    # font sizes
    h1, h2, h3, h4 = 42, 36, 24, 18
    
    # fonts
    fonts_dir = os.path.join(current_dir, 'fonts')
    regular = ttfonts.TTFont('regular', os.path.join(fonts_dir, 'Helvetica.ttf'))
    registerFont(regular)
    bold = ttfonts.TTFont('bold', os.path.join(fonts_dir, 'Helvetica-Bold.ttf'))
    registerFont(bold)
    
    # brand
    cnvs.setFont('bold', h1)
    brand_text = values[0]
    brand_width = stringWidth(brand_text, 'bold', h1, encoding='utf8')
    brand_x = (width - brand_width) // 2
    brand_y = round(height - 2*space)
    cnvs.drawString(brand_x, brand_y, values[0])
    
    # type
    cnvs.setFont('regular', h2)
    type_text = values[1]
    type_width = stringWidth(type_text, 'regular', h2, encoding='utf8')
    type_x = (width - type_width) // 2
    type_y = brand_y - space
    cnvs.drawString(type_x, type_y, values[1])


    # table
    line_y = type_y - space
    cnvs.line(width*0.05, line_y, width-width*0.05, line_y)
    for t in titles:
        idx = titles.index(t)
        if idx in [0, 1, len(titles)-1]: continue
        
        # draw title
        cnvs.setFont('regular', h3)
        title_text = titles[idx]        
        title_x = width*0.1
        title_y = line_y - 0.6*space
        cnvs.drawString(title_x, title_y, title_text)
        
        # draw value
        cnvs.setFont('bold', h3)
        value_text = str(values[idx])
        value_x = width/2 + width*0.05
        value_y = line_y - 0.6*space
        cnvs.drawString(value_x, value_y, value_text)
        
        # draw horizontal line
        line_y -= space
        cnvs.line(width*0.05, line_y, width-width*0.05, line_y)
        
    cnvs.line(width/2, type_y-space, width/2, line_y)
        
    
    # barcode
    code = str(values[-1])
    barcode = Ean13BarcodeWidget(code)
    barcode.barHeight = 42
    
    x0, y0, bw, bh = barcode.getBounds()    
    barcode_drawing = Drawing(bw, bh)
    barcode_drawing.add(barcode)
    
    scale_factor = (width - width*0.1) / bw
    barcode_drawing.scale(scale_factor, scale_factor)
    barcode_x = width * 0.1 / 2
    barcode_y = space
    renderPDF.draw(barcode_drawing, cnvs, barcode_x, barcode_y)
        
    cnvs.save()
    pdf_file.seek(0)
    return pdf_file


def make_all():
    """
    Creates all tags from the XLS
    """
    
    clear_tags()
    source_file = get_file()
    data = get_data(source_file)
    
    titles, values = data['titles'], data['values']
    
    for v in values:
        pdf = make_pdf(titles, v)

        tags_dir = os.path.join(current_dir, 'tags')
        file_name = f'{v[-1]}.pdf'
        file_path = os.path.join(tags_dir, file_name)
        
        with open(file_path, 'wb') as f:
            f.write(pdf.read())
    
    return None


if __name__ == '__main__':
    
    make_all()
